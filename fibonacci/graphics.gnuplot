set terminal pngcairo enhanced font "arial,10" fontscale 1.0 size 1280, 960
#.set terminal png  transparent 
set output 'fibonacci.png'
#set terminal png size 1280, 960
set key left top
set grid
set style data linespoints

set title "Calculation of 10000 Fibonacci numbers (Fn)"
set xlabel "Approximate number n"
set ylabel "Time, s"

file = "AAA.txt"

plot file using 1:2 with lines title 'Iterative' linetype rgb 'blue', \
     file using 1:3 with lines title 'matrix pow' linetype rgb 'red', \
     file using 1:4 with lines title 'Recursive' linetype rgb 'green'

