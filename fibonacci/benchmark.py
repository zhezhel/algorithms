#!/usr/bin/env python3
from fibonacci import matrix, iterative, recursive
from time import time
from random import seed, randint


path = './benchmark.txt'

with open(path, 'w+') as f:
    f.close()


def write(line):
    with open(path, 'a') as f:
        f.writelines(line)


def benchmark():
    seed()
    for around in range(10, 10010, 10):
        ti = 0
        tm = 0
        tr = 0
        count = 10000
        for _ in range(count):
            n = randint(around-5, around+5)
            t = time()
            I = iterative(n)
            t1 = time() - t
            ti += t1

            t = time()
            M = matrix(n)
            t2 = time() - t
            tm += t2

            t = time()
            R = recursive(n)
            t3 = time() - t
            tr += t3

        result = "{0}\t{1}\t{2}\t{3}\n".format(around, ti, tm, tr)
        print(result, end='')
        write(result)


if __name__ == "__main__":
    benchmark()
