#!/usr/bin/env python3
'''
    Three methods for finding the Fibonacci numbers:
    Iterative, Recursive and Matrix power
'''


def matrix(n: int) -> int:
    '''
        Matrix power method for finding the Fibonacci numbers
        >>> matrix(10)
        55
    '''

    if n < 0:
        return None
    Q = [[1, 1],
         [1, 0]]
    tmp = {}

    def multiply_matrices(M1, M2):
        '''
            2x2 matrices multiplication
        '''

        A = [[0, 0],
             [0, 0]]

        A[0][0] = M1[0][0] * M2[0][0] + M1[0][1] * M2[1][0]
        A[0][1] = M1[0][0] * M2[0][1] + M1[0][1] * M2[1][1]
        A[1][0] = M1[1][0] * M2[0][0] + M1[1][1] * M2[1][0]
        A[1][1] = M1[1][0] * M2[0][1] + M1[1][1] * M2[1][1]
        return A

    def get_matrix_power(M, p):
        '''
            Calculate matrix power (p is a power of 2)
        '''

        if p == 1:
            return M
        if p in tmp:
            return tmp[p]
        K = get_matrix_power(M, int(p/2))
        R = multiply_matrices(K, K)
        tmp[p] = R
        return R

    def get_number(n):
        '''
            Get nth Fibonacci number (n is int, non-negative)
        '''

        if n == 0:
            return 0
        if n == 1:
            return 1
        # Decompose to powers of 2,
        # i.e. 62 = 2^5 + 2^4 + 2^3 + 2^2 + 2^0 = 32 + 16 + 8 + 4 + 1.
        powers = [int(pow(2, b))
                  for (b, d) in enumerate(reversed(bin(n-1)[2:])) if d == '1']

        matrices = [get_matrix_power(Q, p) for p in powers]
        while len(matrices) > 1:
            M1 = matrices.pop()
            M2 = matrices.pop()
            R = multiply_matrices(M1, M2)
            matrices.append(R)
        return matrices[0][0][0]

    return get_number(n)


def recursive(n: int) -> int:
    '''
        Recursive method for finding the Fibonacci numbers
        >>> recursive(10)
        55
    '''

    if n < 0:
        return None
    if n > 5:
        from sys import setrecursionlimit
        if n < 10000:
            setrecursionlimit(n * 2 - 1)
        else:
            setrecursionlimit(n + n//100)
    tmp = {0: 0, 1: 1}

    def fib(n):
        if n in tmp:
            return tmp[n]
        tmp[n] = fib(n - 1) + fib(n - 2)
        return tmp[n]
    return fib(n)


def iterative(n):
    '''
        Iterative method for finding the Fibonacci numbers
        >>> iterative(10)
        55
    '''

    if n < 0:
        return None
    if n == 0:
        return 0
    cache = [1, 1]
    n += 1
    for i in range(2, n):
        cache[i % 2] = cache[0] + cache[1]
    return cache[n % 2]

if __name__ == "__main__":
    import doctest
    print(__doc__)
    doctest.testmod()
