To get rid of the problems with recursive implementation of fibonacci numbers
set stack size to unlimited:
* ulimit -s unlimited

![](https://gitlab.com/zhezhel/algorithms/raw/master/fibonacci/fibonacci.png)

